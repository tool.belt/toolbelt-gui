package main

import (
	"bufio"
	"fmt"
	"log"
	"os"

	"gitlab.com/tool.belt/toolbelt-gui/renderers"
	"gitlab.com/tool.belt/toolbelt/definitions"
	"gitlab.com/tool.belt/toolbelt/environment"
	"gitlab.com/tool.belt/toolbelt/tools"
)

func main() {
	opts := environment.GetOptions(os.Args[1:])

	if opts.ToolArg == "" {
		g := renderers.New()
		err := g.Run()
		if err != nil {
			log.Fatalf("%v", err)
		}

		return
	}

	exitCode := 0

	defs, err := definitions.Load()
	if err != nil {
		log.Fatalf("%v", err)
	}

	exitCode, err = tools.ExecTool(defs.GetDefByArg(opts.ToolArg), opts.ExtraArguments)
	if err != nil {
		log.Fatalf("%v", err)
	}

	// TODO consider making this configurable
	fmt.Print("\nPress ENTER to continue.")
	input := bufio.NewScanner(os.Stdin)
	input.Scan()

	os.Exit(exitCode)
}
