class Tool extends React.Component {

    handleClick = () => {
        RunCommand(this.props.def.Idx);
    }

    render() {
        var name = this.props.def.Name.toUpperCase();
        var help = this.props.def.Help.toUpperCase();

        return (
            <div className="tool" onClick={this.handleClick}>
                <div>{name}</div>
                <div>HELP: {help}</div>
            </div>
        );
    }
}

function Tools(props) {
    var tools = props.defs.map((def, i) => {
        return (
            <Tool key={i} def={def}></Tool>
        );
    });

    return (
        <div className="tools">
            {tools}
        </div>
    );
}

class Refresh extends React.Component {

    handleClick = () => {
        var self = this;

        self.props.populateDefs();
    }

    render() {

        return (
            <div className="refresh" onClick={this.handleClick}>
            </div>
        );
    }
}

class Header extends React.Component {
    render() {
        return (
            <header>
                <h1><span>TOOL</span>BELT</h1>
                <Refresh populateDefs={this.props.populateDefs} />
            </header>
        );
    }
}

class Toolbelt extends React.Component {
    state = {
        Defs: []
    }

    componentDidMount() {
        this.populateDefs();
    }

    populateDefs() {
        var self = this;

        LoadDefs().then(function (defs) {
            self.setState({ Defs: defs });
        });
    }

    render() {
        return <div className="toolbelt">
            <Header populateDefs={this.populateDefs.bind(this)} />
            <Tools defs={this.state.Defs}></Tools>
        </div>
    }
}

// ========================================

ReactDOM.render(
    <Toolbelt />,
    document.getElementById('root')
);
