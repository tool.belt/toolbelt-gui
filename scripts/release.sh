#!/usr/bin/env bash

RELEASE_NAME=$1
COMMIT=$2
PROJECT_ID=$3
PRIVATE_TOKEN=$4

# Tag Commit and push to repo
git tag -a $RELEASE_NAME $COMMIT

git push origin master $RELEASE_NAME

# Upload files to gitlab
UPLOAD_OUTPUT=`curl --request POST --header "Private-Token: ${PRIVATE_TOKEN}" --form "file=@./build/toolbelt-gui-linux-amd64" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/uploads"`
LINUX_URL=`echo ${UPLOAD_OUTPUT} | grep -m1 -oP '"url"\s*:\s*"\K[^"]+'`

UPLOAD_OUTPUT=`curl --request POST --header "Private-Token: ${PRIVATE_TOKEN}" --form "file=@./build/toolbelt-gui-darwin-amd64" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/uploads"`
DARWIN_URL=`echo ${UPLOAD_OUTPUT} | grep -m1 -oP '"url"\s*:\s*"\K[^"]+'`

UPLOAD_OUTPUT=`curl --request POST --header "Private-Token: ${PRIVATE_TOKEN}" --form "file=@./build/toolbelt-gui-windows-amd64.exe" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/uploads"`
WINDOWS_URL=`echo ${UPLOAD_OUTPUT} | grep -m1 -oP '"url"\s*:\s*"\K[^"]+'`

# Get Description and create release
DESCRIPTION=`sed -zE 's/\r\n|\n/\\\\n/g' < ./release_template.md`

curl \
     --header 'Content-Type: application/json' --header "Private-Token: ${PRIVATE_TOKEN}" \
     --data "{\"name\": \"${RELEASE_NAME}\", \"tag_name\": \"${RELEASE_NAME}\", \"description\": \"${DESCRIPTION}\", \
     \"assets\": { \"links\": [\
     { \"name\": \"toolbelt-gui-windows-amd64.exe\", \"url\": \"https://gitlab.com/tool.belt/toolbelt-gui${WINDOWS_URL}\" },\
     { \"name\": \"toolbelt-gui-linux-amd64\", \"url\": \"https://gitlab.com/tool.belt/toolbelt-gui${LINUX_URL}\" },\
     { \"name\": \"toolbelt-gui-darwin-amd64\", \"url\": \"https://gitlab.com/tool.belt/toolbelt-gui${DARWIN_URL}\" }\
     ] }}" --request POST "https://gitlab.com/api/v4/projects/${PROJECT_ID}/releases"
