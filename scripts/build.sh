#!/usr/bin/env bash

echo "Building for Linux"
env GOOS=linux GOARCH=amd64 go build -o build/toolbelt-gui-linux-amd64

echo "Building for Windows"
env GOOS=windows GOARCH=amd64 go build -o build/toolbelt-gui-windows-amd64.exe

echo "Building for Mac OSX"
env GOOS=darwin GOARCH=amd64 go build -o build/toolbelt-gui-darwin-amd64