module gitlab.com/tool.belt/toolbelt-gui

go 1.14

require (
	github.com/gobuffalo/packr/v2 v2.8.0
	github.com/zserge/lorca v0.1.9
	gitlab.com/tool.belt/toolbelt v0.0.2
)