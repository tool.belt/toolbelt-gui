package runner

import (
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/tool.belt/toolbelt/definitions"
)

func Exec(def definitions.Definition) {
	ex, err := os.Executable()
	if err != nil {
		fmt.Println(err)
		return
	}

	wd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		return
	}

	// TODO: add information about needing to change terminal preferences to close window on shell exit to readme
	cmd := exec.Command("osascript", "-s", "h", "-e", `tell app "Terminal" to activate`, "-e",
		fmt.Sprintf(`tell app "Terminal" to do script "cd '%s'; %s -%s; exit"`, wd, ex, def.Argument))
	cmd.Run()
}
