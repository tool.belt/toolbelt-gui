package runner

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"

	"gitlab.com/tool.belt/toolbelt/definitions"
)

func Exec(def definitions.Definition) {
	ex, err := os.Executable()
	if err != nil {
		fmt.Println(err)
		return
	}

	wd, err := os.Getwd()
	if err != nil {
		fmt.Println(err)
		return
	}

	cmd := exec.Command("cmd.exe")
	cmd.SysProcAttr = &syscall.SysProcAttr{}
	cmd.SysProcAttr.CmdLine = "cmd.exe /c start " + fmt.Sprintf(`%s -%s`, ex, def.Argument)
	cmd.Dir = wd
	cmd.Run()
}
