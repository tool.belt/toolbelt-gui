package renderers

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/gobuffalo/packr/v2"
	"github.com/zserge/lorca"
	"gitlab.com/tool.belt/toolbelt-gui/runner"
	"gitlab.com/tool.belt/toolbelt/definitions"
)

type gui struct {
	defs []definitions.Definition
}

func New() *gui {
	return &gui{}
}

func (g *gui) Run() error {
	ui, err := lorca.New("", "", 640, 640)
	if err != nil {
		return err
	}

	defer ui.Close()

	instrumentSignals(ui)
	g.bindMethods(ui)
	if err := runUIServer(ui); err != nil {
		return err
	}

	<-ui.Done()

	return nil
}

func (g *gui) bindMethods(ui lorca.UI) {
	ui.Bind("LoadDefs", func() ([]definitions.Definition, error) {
		defs, err := definitions.Load()
		if err == nil {
			g.defs = defs
		}

		return defs, err
	})

	ui.Bind("RunCommand", func(option int) {
		go runner.Exec(g.defs[option])
	})
}

func instrumentSignals(ui lorca.UI) {
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		ui.Close()
	}()
}

func runUIServer(ui lorca.UI) error {
	p, err := findFreePort()
	if err != nil {
		return err
	}

	port := fmt.Sprint(p)

	hostURL := "http://localhost:" + port
	go serveHTML(port)
	return ui.Load(hostURL)
}

func serveHTML(port string) {
	box := packr.New("static", "../static")

	http.Handle("/", http.FileServer(box))

	log.Printf("Serving ui on HTTP port: %s\n", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func findFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}

	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}
